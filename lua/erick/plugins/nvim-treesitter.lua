return {
	"nvim-treesitter/nvim-treesitter",
	build = ":TSUpdate",
	event = { "BufReadPre", "BufNewFile" },
	dependencies = {
		"nvim-treesitter/nvim-treesitter-textobjects",
	},
	opts = {
		ensure_installed = {
			"astro",
			"bash",
			"css",
			"html",
			"http",
			"java",
			"javascript",
			"json",
			"lua",
			"luadoc",
			"markdown",
			"markdown_inline",
			"php",
			"php_only",
			"blade",
			"query",
			"sql",
			"toml",
			"tsx",
			"typescript",
			"vim",
			"vimdoc",
			"vue",
			"xml",
			"yaml",
		},
		highlight = {
			enable = true,
			additional_vim_regex_highlighting = false,
			disable = function(_, buf)
				local max_filesize = 100 * 1024 -- 100 KB
				---@diagnostic disable-next-line: undefined-field
				local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
				if ok and stats and stats.size > max_filesize then
					return true
				end
			end,
		},
		indent = {
			enable = true,
		},
		incremental_selection = {
			enable = true,
			keymaps = {
				init_selection = "<C-space>",
				node_incremental = "<C-space>",
				node_decremental = "<C-space>-",
				scope_incremental = nil,
			},
		},
		textobjects = {
			select = {
				enable = true,
				lookahead = true,
				keymaps = {
					["af"] = "@function.outer",
					["if"] = "@function.inner",
					["aC"] = "@class.outer",
					["iC"] = "@class.inner",
					["ac"] = "@conditional.outer",
					["ic"] = "@conditional.inner",
					["al"] = "@loop.outer",
					["il"] = "@loop.inner",
				},
			},
		},
	},
	config = function(_,opts)
		require("nvim-treesitter.parsers").get_parser_configs().blade = {
			install_info = {
				url = "https://github.com/EmranMR/tree-sitter-blade",
				files = { "src/parser.c" },
				branch = "main",
			},
			filetype = "blade",
		}
		vim.filetype.add({ pattern = { [".*%.blade%.php"] = "blade" } })
		vim.cmd([[
        augroup BladeFiletypeRelated
        autocmd!
        autocmd BufNewFile,BufRead *.blade.php setfiletype blade
        augroup END
        ]])

		vim.cmd([[
        augroup BladeIndentation
        autocmd!
        autocmd FileType blade setlocal tabstop=2 shiftwidth=2 expandtab
        augroup END
        ]])

		require("nvim-treesitter.configs").setup(opts)
	end,
}
