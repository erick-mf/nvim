return {
	"iamcco/markdown-preview.nvim",
	enabled = false,
	build = function()
		vim.fn["mkdp#util#install"]()
	end,
	ft = "markdown",
	config = function()
		vim.g.mkdp_auto_start = 1
	end,
}
