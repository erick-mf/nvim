vim.g.mapleader = " "
local map = vim.keymap.set

-- Splits
map("n", "<leader>qq", ":bd!<CR>", { noremap = true, silent = true, desc = "Splits quit" })
map("n", "<C-q>", ":bd<CR>", { noremap = true, silent = true, desc = "Splits quit" })
map("n", "<A-v>", ":vnew +set\\ buftype=nofile<CR>", { noremap = true, silent = true, desc = "Splits vertical" })
map("n", "<A-s>", ":new +set\\ buftype=nofile<CR>", { noremap = true, silent = true, desc = "Splits horizontal" })

-- windows
map("n", "<C-t>", ":tabnew<CR>", { noremap = true, silent = true, desc = "tabnew" })
map("n", "<C-h>", "<C-w>h", { noremap = true, silent = true, desc = "Change window left" })
map("n", "<C-j>", "<C-w>j", { noremap = true, silent = true, desc = "Change window down" })
map("n", "<C-k>", "<C-w>k", { noremap = true, silent = true, desc = "Change window up" })
map("n", "<C-l>", "<C-w>l", { noremap = true, silent = true, desc = "Change window right" })
map("n", "<C-w>h", "<C-w>H", { noremap = true, silent = true, desc = "split move left" })
map("n", "<C-w>j", "<C-w>J", { noremap = true, silent = true, desc = "split move down" })
map("n", "<C-w>k", "<C-w>K", { noremap = true, silent = true, desc = "split move up" })
map("n", "<C-w>l", "<C-w>L", { noremap = true, silent = true, desc = "split move right" })
map("n", "<C-n>", "<cmd>bnext<cr>", { noremap = true, silent = true, desc = "Tab next" })
map("n", "<C-p>", "<cmd>bprev<cr>", { noremap = true, silent = true, desc = "Tab prev" })

-- indentar codigo
vim.keymap.set({ "n", "v" }, "<leader>F", function()
	-- Guardar la posición actual del cursor
	local cursor_pos = vim.api.nvim_win_get_cursor(0)
	-- Seleccionar todo el documento en modo visual
	vim.cmd("normal! ggVG")
	-- Indentar la selección
	vim.cmd("normal! =")
	-- Volver a la posición original del cursor
	vim.api.nvim_win_set_cursor(0, cursor_pos)
end, { noremap = true, silent = true, desc = "Indent entire file" })

-- mover las lineas selecionadas arriba/abajo
map("v", "J", ":m '>+1<CR>gv=gv", { noremap = true, silent = true })
map("v", "K", ":m '<-2<CR>gv=gv", { noremap = true, silent = true })

-- para la busqueda
map("n", "n", "nzzzv", {})
map("n", "N", "Nzzzv", {})

map("n", "<Esc>", ":noh<return><esc>", { silent = true })
map({ "n", "i" }, "<C-c>", "<Esc>", { silent = true })
map("n", "space", "<nop>", {})
map("n", "Q", "<nop>", {})
map("n", "q", "<nop>", {})
map("n", "J", "<nop>", {})
map("n", "<C-z>", "<nop>", {})
map("n", "<leader>e", "<cmd>Explore<cr>", { silent = true, desc = "Explorer" })

--desplazamiento suave
map({ "n", "v" }, "<C-u>", "5k", { noremap = true, silent = true })
map({ "n", "v" }, "<C-d>", "5j", { noremap = true, silent = true })

-- agregar indentación al codigo
map("v", "<", "<gv", { noremap = true, silent = true })
map("v", ">", ">gv", { noremap = true, silent = true })

-- modifica el texto donde el cursor se encuentre
map("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

-- H and L go to begining/end of line
map({ "n", "v" }, "H", "^", { desc = "Go to Beginning of Line" })
map({ "n", "v" }, "L", "$", { desc = "Go to End of Line" })

map({ "n", "v" }, "<leader>y", '"zy', { noremap = true, silent = true, desc = "copiar al registro z" })
map({ "n", "v" }, "<leader>p", '"zp', { noremap = true, silent = true, desc = "pegar del registro z después" })
map("t", "<Esc><Esc>", "<C-\\><C-n>", { noremap = true, silent = true })

map("n", "<leader>qa", function()
	local old_format = vim.g.autoformat
	vim.g.autoformat = false

	vim.cmd("wa")

	local current_buf = vim.api.nvim_get_current_buf()
	for _, buf in ipairs(vim.api.nvim_list_bufs()) do
		if buf ~= current_buf and vim.api.nvim_buf_is_valid(buf) then
			vim.api.nvim_buf_delete(buf, { force = true })
		end
	end

	vim.g.autoformat = old_format
end, { noremap = true, silent = true, desc = "Close all buffers except current" })
