return {
	"neovim/nvim-lspconfig",
	event = { "BufReadPre", "BufNewFile" },
	dependencies = {
		"williamboman/mason.nvim",
		{
			"folke/lazydev.nvim",
			ft = "lua",
			opts = {
				library = {
					{ path = "${3rd}/luv/library", words = { "vim%.uv" } },
				},
			},
		},
	},
	config = function()
		require("lazydev").setup()

		local lspconfig = require("lspconfig")
		local capabilities = require("cmp_nvim_lsp").default_capabilities()
		local map = vim.keymap.set

		vim.api.nvim_create_autocmd("LspAttach", {
			group = vim.api.nvim_create_augroup("UserLspConfig", {}),
			callback = function(ev)
				vim.bo[ev.buf].omnifunc = "v:lua.vim.lsp.omnifunc"
				local opts = { buffer = ev.buf, silent = true, noremap = true }

				opts.desc = "Show LSP references"
				map("n", "gr", "<cmd>FzfLua lsp_references<CR>", opts)

				opts.desc = "Show LSP definitions"
				map("n", "gd", vim.lsp.buf.definition, opts)

				opts.desc = "See available code actions"
				map({ "n", "v" }, "<leader>vc", vim.lsp.buf.code_action, opts)

				opts.desc = "Smart rename"
				map("n", "<leader>rn", vim.lsp.buf.rename, opts)

				opts.desc = "Format code"
				map("n", "<A-f>", vim.lsp.buf.format, opts)

				opts.desc = "Show line diagnostics"
				map("n", "<leader>vd", vim.diagnostic.open_float, opts)

				opts.desc = "Show documentation for what is under cursor"
				map("n", "K", vim.lsp.buf.hover, opts)
			end,
		})

		local servers = {
			lua_ls = {
				settings = {
					Lua = {
						runtime = { version = "LuaJIT" },
						diagnostics = { globals = { "vim" } },
						workspace = {
							library = {
								[vim.fn.expand("$VIMRUNTIME/lua")] = true,
								[vim.fn.stdpath("config") .. "/lua"] = true,
							},
							checkThirdParty = false,
						},
						telemetry = { enable = false },
						hint = { enable = false },
					},
				},
			},
			eslint = {
				on_attach = function(_, bufnr)
					vim.api.nvim_create_autocmd("BufWritePre", {
						buffer = bufnr,
						command = "EslintFixAll",
					})
				end,
				settings = {
					workingDirectory = { mode = "auto" },
				},
			},
			phpactor = {
				root_dir = lspconfig.util.root_pattern("composer.json", ".git", "*.php"),
				filetypes = { "php", "blade" },
				init_options = {
					["language_server_configuration.auto_config"] = true,
					["language_server_worse_reflection.inlay_hints.enable"] = true,
					["language_server_worse_reflection.inlay_hints.types"] = false,
					["language_server_worse_reflection.inlay_hints.params"] = true,
					["code_transform.import_globals"] = false,
					["language_server_phpstan.mem_limit"] = "2048M",
					["language_server.diagnostic_ignore_codes"] = {
						"worse.docblock_missing_return_type",
						"worse.missing_return_type",
						"worse.docblock_missing_param",
						"fix_namespace_class_name",
						"worse.missing_member",
					},
					["indexer.exclude_patterns"] = {
						"/vendor/**/Tests/**/*",
						"/vendor/**/tests/**/*",
						"/var/cache/**/*",
						"/vendor/composer/**/*",
						"/vendor/composer/**/*",
						"/vendor/laravel/fortify/workbench/**/*",
						"/vendor/filament/forms/.stubs.php",
						"/vendor/filament/notifications/.stubs.php",
						"/vendor/filament/tables/.stubs.php",
						"/vendor/filament/actions/.stubs.php",
						"/storage/framework/cache/**/*",
						"/storage/framework/views/**/*",
					},
				},
				handlers = {
					["textDocument/publishDiagnostics"] = function(err, result, ctx, config)
						-- Filtra los diagnósticos
						result.diagnostics = vim.tbl_filter(function(diagnostic)
							-- Verifica si el archivo está en el directorio de vistas
							if
								string.match(result.uri, "/[vV]i[es][wt][as]?/")
								or string.match(result.uri, "/config?/")
								or string.match(result.uri, "console.php")
							then
								-- Elimina los avisos de variables no definidas en vistas
								if
									string.match(diagnostic.message, "Undefined variable")
									or string.match(diagnostic.message, "Fix PSR namespace and class name")
								then
									return false
								end
							end

							return true
						end, result.diagnostics)

						-- Llama al handler estándar con los diagnósticos filtrados
						vim.lsp.handlers["textDocument/publishDiagnostics"](err, result, ctx, config)
					end,
				},
			},
			angularls = {},
			astro = {},
			cssls = {},
			emmet_ls = {},
			tailwindcss = {},
			html = {},
			jdtls = {},
			jsonls = {},
			lemminx = {},
			ts_ls = {},
			volar = {},
		}

		local setup_server = function(server, config)
			config = config or {}
			config.capabilities = vim.tbl_deep_extend("force", {}, capabilities, config.capabilities or {})
			lspconfig[server].setup(config)
		end

		for server, config in pairs(servers) do
			if server ~= "jdtls" then
				setup_server(server, config)
			end
		end
	end,
}
