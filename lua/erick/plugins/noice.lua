return {
	"folke/noice.nvim",
	dependencies = {
		"MunifTanjim/nui.nvim",
		"rcarriga/nvim-notify",
	},
	event = "VeryLazy",
	opts = {
		lsp = {
			-- override markdown rendering so that **cmp** and other plugins use **Treesitter**
			override = {
				["vim.lsp.util.convert_input_to_markdown_lines"] = true,
				["vim.lsp.util.stylize_markdown"] = true,
				["cmp.entry.get_documentation"] = true,
			},
			hover = {
				enable = true,
				silent = true,
			},
			signature = {
				enable = true,
				silent = true,
			},
		},
		-- -- you can enable a preset for easier configuration
		presets = {
			bottom_search = true,
			command_palette = true,
			long_message_to_split = true,
			inc_rename = true,
			lsp_doc_border = true,
		},
		cmdline = {
			view = "cmdline",
		},
		messages = {
			enabled = true, -- enables the Noice messages UI
			view = "notify", -- default view for messages
			view_error = "notify", -- view for errors
			view_warn = "notify", -- view for warnings
			view_history = "messages", -- view for :messages
			view_search = "virtualtext", -- view for search count messages. Set to `false` to disable
		},
		routes = {
			{
				view = "split",
				filter = { event = "msg_show", min_height = 5 },
			},
			{
				view = "split",
				filter = { event = "notify", min_height = 5 },
			},
			{
				view = "split",
				filter = { cmdline = "Notifications" },
			},
		},
	},
}
