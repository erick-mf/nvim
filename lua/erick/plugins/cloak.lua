return {
	"laytan/cloak.nvim",
	cmd = "CloakEnable",
	opts = {
		cloak_character = "*",
		highlight_group = "Comment",
		patterns = {
			{
				file_pattern = ".env*",
				cloak_pattern = { "=.+", ":.+" },
			},
		},
	},
	config = function(opts)
		require("cloak").setup(opts)
	end,
	init = function()
		vim.api.nvim_create_autocmd("BufEnter", {
			group = vim.api.nvim_create_augroup("GroupErick", { clear = true }),
			pattern = ".env",
			callback = function()
				vim.cmd([[CloakEnable]])
			end,
		})
	end,
	keys = {
		{ "<leader>cd", "<cmd>CloakPreviewLine<cr>", desc = "CloakDisable" },
	},
}
