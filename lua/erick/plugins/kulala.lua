return {
	"mistweaverco/kulala.nvim",
	init = function()
		vim.filetype.add({
			extension = {
				["http"] = "http",
			},
		})
	end,
	ft = "http",
	opts = {
		default_view = "body",
		debug = false,
		default_winbar_panes = { "body" },
	},
	keys = {
		{
			"<leader>ht",
			function()
				require("kulala").run()
			end,
			desc = "Run Kulala",
		},
	},
}
