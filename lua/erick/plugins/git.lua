return {
	{
		"tpope/vim-fugitive",
		enabled = false,
		keys = {
			{ "<leader>G", "<cmd>Git<cr>", desc = "Git" },
		},
		cmd = { "G", "Git" },
	},
	{
		"NeogitOrg/neogit",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"sindrets/diffview.nvim",
		},
		cmd = "Neogit",
		keys = {
			{ "<leader>G", "<cmd>Neogit cwd=%:p:h<cr>", desc = "Git commit" },
		},
		config = true,
	},
}
