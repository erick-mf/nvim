return {
	{
		"olimorris/onedarkpro.nvim",
		lazy = false,
		priority = 1000,
		config = function()
			require("onedarkpro").setup({
				styles = {
					comments = "italic",
					keywords = "italic",
				},
				options = {
					transparency = true,
				},
			})
			-- vim.cmd.colorscheme("onedark")
		end,
	},
	{
		"rose-pine/neovim",
		name = "rose-pine",
		lazy = false,
		priority = 1000,
		config = function()
			require("rose-pine").setup({
				styles = {
					bold = false,
					italic = true,
					transparency = true,
				},
				palette = {
					moon = {
						base = "#18191a",
						overlay = "#363738",
					},
				},
			})
			-- vim.cmd.colorscheme("rose-pine-moon")
		end,
	},
	{
		"folke/tokyonight.nvim",
		lazy = false,
		priority = 1000,
		config = function()
			require("tokyonight").setup({
				transparent = true,
				styles = {
					comments = { italic = true },
					keywords = { italic = true },
					floats = "transparent",
				},
				on_colors = function(colors)
					colors.hint = colors.orange
					colors.error = "#ff0000"
				end,
			})
			-- vim.cmd.colorscheme("tokyonight-moon")
		end,
	},
	{
		"ribru17/bamboo.nvim",
		lazy = false,
		priority = 1000,
		config = function()
			require("bamboo").setup({
				transparent = true,
				styles = {
					code_style = {
						comments = { italic = true },
						keywords = { italic = true },
					},
				},
			})

			-- vim.cmd.colorscheme("bamboo")
		end,
	},
	{
		"rebelot/kanagawa.nvim",
		lazy = false,
		priority = 1000,
		config = function()
			require("kanagawa").setup({
				commentStyle = { italic = true },
				keywordStyle = { italic = true },
				transparent = true,
				colors = {
					theme = {
						all = {
							ui = {
								bg_gutter = "none",
							},
						},
					},
				},
			})
			vim.cmd.colorscheme("kanagawa-wave")
		end,
	},
	{
		"catppuccin/nvim",
		name = "catppuccin",
		lazy = false,
		priority = 1000,
		opts = {
			no_italic = true,
			term_colors = true,
			transparent_background = false,
			styles = {
				comments = { italic = true },
				conditionals = {},
				loops = {},
				functions = {},
				keywords = { italic = true },
				strings = {},
				variables = {},
				numbers = {},
				booleans = {},
				properties = {},
				types = {},
			},
			color_overrides = {
				mocha = {
					base = "#000000",
					mantle = "#000000",
					crust = "#000000",
				},
			},
			integrations = {
			},
		},
		config = function(opts)
			require("catppuccin").setup(opts)
			-- vim.cmd.colorscheme("catppuccin-mocha")
		end,
	},
}
