return {
	"nvimtools/none-ls.nvim",
	dependencies = {
		"nvimtools/none-ls-extras.nvim",
	},
	event = { "BufReadPre", "BufNewFile" },
	config = function()
		local null_ls = require("null-ls")

		local formatting = null_ls.builtins.formatting
		local diagnostics = null_ls.builtins.diagnostics

		local augroup = vim.api.nvim_create_augroup("LspFormatting", {})

		null_ls.setup({
			sources = {
				null_ls.builtins.completion.spell,
				null_ls.builtins.completion.tags,
				null_ls.builtins.code_actions.refactoring,
				formatting.prettier.with({ args = { "--stdin-filepaht", "--print-width", "120" } }),
				formatting.pint,
				formatting.stylua,
				formatting.blade_formatter,
				formatting.sqlfluff.with({ extra_args = { "--dialect", "mysql" } }),
				diagnostics.sqlfluff.with({ extra_args = { "--dialect", "mysql" } }),
				formatting.sqlfluff.with({ extra_args = { "--dialect", "sqlite" } }),
				diagnostics.sqlfluff.with({ extra_args = { "--dialect", "sqlite" } }),
				-- diagnostics.phpcs,
				-- require("none-ls.diagnostics.eslint"),
			},

			-- configure format on save
			on_attach = function(current_client, bufnr)
				if current_client.supports_method("textDocument/formatting") then
					vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
					vim.api.nvim_create_autocmd("BufWritePost", {
						group = augroup,
						buffer = bufnr,
						callback = function()
							vim.lsp.buf.format({
								bufnr = bufnr,
								timeout_ms = 10000,
								async = true,
								filter = function(client)
									--  only use null-ls for formatting instead of lsp server
									return client.name == "null-ls"
								end,
							})
						end,
					})
				end
			end,
		})
	end,
}
