return {
	"catgoose/nvim-colorizer.lua",
	filetypes = { "html", "php", "blade", "javascript", "typescript", "css" },
	event = "BufReadPre",
	config = function()
		require("colorizer").setup({
			filetypes = { "html", "php", "javascript", "typescript", "css" },
			user_default_options = {
				RGB = true,
				RRGGBB = true,
				names = true,
				RRGGBBAA = true,
				AARRGGBB = true,
				rgb_fn = true,
				hsl_fn = true,
				css = true,
				css_fn = true,
				mode = "background",
				tailwind = "both",
				sass = { enable = true, parsers = { "css" } },
				virtualtext = "■",
			},
		})
	end,
}
